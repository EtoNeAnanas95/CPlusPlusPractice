#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
using namespace std;
class MergeClass
{
private:
    void Merge(vector<int>& array, vector<int>& left, vector<int>& right) {
        auto i = 0;
        auto j = 0;
        auto k = 0;
        while (i < left.size() && j < right.size()) {
            if (left[i] <= right[j]) array[k++] = left[i++];
            else array[k++] = right[j++];
        }

        while (i < left.size()) {
            array[k++] = left[i++];
        }
        while (j < right.size()) {
            array[k++] = right[j++];
        }
    }
    
public:
    void Sort(vector<int>& array) {
         if (array.size() <= 1) return;

        auto mid = array.size() / 2;
        vector<int> left(array.begin(), array.begin() + mid);
        vector<int> right(array.begin() + mid, array.end());

        auto leftSort = [&left, this]() {
            Sort(left);
        };
        thread goSortLeft(leftSort);
        
        auto rightSort = [&right, this]() {
            Sort(right);
        };
        thread goSortRight(rightSort);
        goSortLeft.join();
        goSortRight.join();
        Merge(array, left, right);
    }
};