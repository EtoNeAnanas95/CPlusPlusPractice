#include <iostream>
#include <fcntl.h>
#include <Windows.h>
#include <string>
#include "MergeClass.cpp"
#include <vector>

#define RESET   "\033[0m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define PINK    "\033[38;5;206m"
#define MAGENTA "\033[35m"
#define CYAN    "\033[36m"

using namespace std;
int main() {
    _setmode(_fileno(stdout), _O_U16TEXT);
    setlocale(LC_ALL, "Rus");
    SetConsoleCP(1251);

    system("cls");
    vector<int> array;
    auto g = 0;
    for (int i = 0; i < 10; i++) {
        wcout << YELLOW << L"Введите " << ++g << L" элемент массива" << RESET << endl;
        int value;
        wcin >> value; 
        array.push_back(value);
    }
    wcout << endl;
    MergeClass sortClass;
    auto sorting = [&array, &sortClass]() { sortClass.Sort(array); };
    thread goSort(sorting);
    goSort.join();
    g = 0;
    for (auto i : array) {
        wcout << PINK << ++g << L" ячейка массива" << GREEN << L" равна " << i << RESET << endl;
    }
}