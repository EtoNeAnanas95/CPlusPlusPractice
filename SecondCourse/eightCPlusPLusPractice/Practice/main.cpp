#include <iostream>
#include <fcntl.h>
#include <Windows.h>
#include <string>

using namespace std;
int main() {
    _setmode(_fileno(stdout), _O_U16TEXT);
    setlocale(LC_ALL, "Rus");
    SetConsoleCP(1251);

    system("cls");
    wcout << L"Введите основной текст" << endl;
    wstring mainSentence;
    getline(wcin, mainSentence);

    wcout << L"Введите поисковой текст" << endl;
    wstring subSentence;
    getline(wcin, subSentence);

    HINSTANCE load = LoadLibraryW(L"libeightCPlusPlusPractice.dll");

    typedef void (*findStr)(wstring, wstring);
    findStr Find = (findStr)GetProcAddress(load, "checkCentence");

    Find(subSentence, mainSentence);
    FreeLibrary(load);
}