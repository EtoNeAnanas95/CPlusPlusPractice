#include <Arduino.h>

#define leds 11
#define btn1 13
#define btn2 12
#define btn3 8

void setup()
{
  pinMode(leds, OUTPUT);
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);

  Serial.begin(10000);
  Serial.println("---------------------------------------");
  Serial.println("Ciscircuit is runing");
  Serial.println("");
}

void cringeOutput(String message)
{
  for (auto symbol : message)
  {
    for (int j = 7; j >= 0; j--) Serial.print((symbol >> j) & 1);
    Serial.print(" ");
  }
}

bool ledState = LOW;
byte brightnessLed = 0;
bool blinkState = false;

void loop()
{
  if (blinkState)
  {
    digitalWrite(leds, LOW);
    delay(150);
    digitalWrite(leds, HIGH);
    delay(150);
  }

  if (digitalRead(btn1) == HIGH)
  {
    blinkState = false;
    Serial.println("first button pressed");
    if (ledState == LOW)
    {
      Serial.println("lamp on");
      ledState = HIGH;
      digitalWrite(leds, HIGH);
    }
    else
    {
      ledState = LOW;
      Serial.println("lamp off");
      digitalWrite(leds, LOW);
    }
    while(digitalRead(btn1)) {}
    return;
  }
  if (digitalRead(btn2) == HIGH)
  {
    blinkState = false;
    Serial.println("second button pressed");
    if (brightnessLed < 255)
    {
      ledState = HIGH;
      Serial.println("increase led brightness");
      brightnessLed = brightnessLed + 51;
    }
    else
    {
      ledState = LOW;
      Serial.println("turn off leds");
      brightnessLed = 0;
    }
    Serial.println("Change brightness " + String(brightnessLed));
    analogWrite(leds, brightnessLed);
    ledState = HIGH;
    while(digitalRead(btn2)) {}
    return;
  }
  if (digitalRead(btn3) == HIGH)
  {
    Serial.println("thrid button pressed");
    blinkState = false;
    Serial.println("Кирилов Дмитрий Сергеевич");
    cringeOutput("Кирилов");
    while(digitalRead(btn3)) {}
    return;
  }
  if (Serial.available() > 0)
  {
    char symbol = Serial.read();
    if (symbol == 'S')
    {
      blinkState = false;
      analogWrite(leds, 0);
    }
    else if (symbol == 'M') blinkState = true;
    return;
  }
  delay(200);
}