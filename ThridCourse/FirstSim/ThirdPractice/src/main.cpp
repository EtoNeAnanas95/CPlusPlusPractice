﻿#include "Arduino.h"
#include <EEPROM.h>

#define led1 13
#define led2 12
#define led3 11
#define led4 10

#define btn1 7
#define btn2 4
#define btn3 2

short btn1State = 0;
short btn2State = 0;

void setup()
{
  EEPROM.get(0, btn1State);
  EEPROM.get(1, btn2State);

  Serial.begin(9600);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);

  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);
}

void loop()
{
  if (btn1State % 2 == 0)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
  }
  else
  {
    digitalWrite(led1, LOW);
    digitalWrite(led2, HIGH);
  }

  if (btn2State % 2 == 0)
  {
    digitalWrite(led3, HIGH);
    digitalWrite(led4, LOW);
  }
  else
  {
    digitalWrite(led3, LOW);
    digitalWrite(led4, HIGH);
  }

  if (digitalRead(btn1))
  {
    btn1State++;
    EEPROM.put(0, btn1State);
    Serial.println("Increment btn1State: " + String(btn1State));
    while (digitalRead(btn1))
    {
    }
  }
  if (digitalRead(btn2))
  {
    btn2State++;
    EEPROM.put(2, btn2State);
    Serial.println("Increment btn2State: " + String(btn2State));
    while (digitalRead(btn2))
    {
    }
  }
  if (digitalRead(btn3))
  {
    btn1State = 0;
    btn2State = 0;
    EEPROM.put(0, btn1State);
    EEPROM.put(2, btn2State);
    Serial.println("Reset all states");
    while (digitalRead(btn3))
    {
    }
  }

  delay(200);
}
