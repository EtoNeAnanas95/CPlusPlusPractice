#include <Arduino.h>
#include "pitches.h"

#define led 2
#define firstButton 7
#define secondButton 4
#define buzzer 11

void setup() {
  pinMode(led, OUTPUT);
  pinMode(firstButton, INPUT);
  pinMode(secondButton, INPUT);
  Serial.begin(9600);
}

int blink;
bool isBlink = false;

int melody[] = {
  NOTE_E5,8, NOTE_E5,8, REST,8, NOTE_E5,8, REST,8, NOTE_C5,8, NOTE_E5,8, 
  NOTE_G5,4, REST,4, NOTE_G4,8, REST,4, 
  NOTE_C5,-4, NOTE_G4,8, REST,4, NOTE_E4,-4,
  NOTE_A4,4, NOTE_B4,4, NOTE_AS4,8, NOTE_A4,4,
  NOTE_G4,-8, NOTE_E5,-8, NOTE_G5,-8, NOTE_A5,4, NOTE_F5,8, NOTE_G5,8,
  REST,8, NOTE_E5,4,NOTE_C5,8, NOTE_D5,8, NOTE_B4,-4,
  NOTE_C5,-4, NOTE_G4,8, REST,4, NOTE_E4,-4, 
  NOTE_A4,4, NOTE_B4,4, NOTE_AS4,8, NOTE_A4,4,
  NOTE_G4,-8, NOTE_E5,-8, NOTE_G5,-8, NOTE_A5,4, NOTE_F5,8, NOTE_G5,8,
  REST,8, NOTE_E5,4,NOTE_C5,8, NOTE_D5,8, NOTE_B4,-4,

  REST,4, NOTE_G5,8, NOTE_FS5,8, NOTE_F5,8, NOTE_DS5,4, NOTE_E5,8,
  REST,8, NOTE_GS4,8, NOTE_A4,8, NOTE_C4,8, REST,8, NOTE_A4,8, NOTE_C5,8, NOTE_D5,8,
  REST,4, NOTE_DS5,4, REST,8, NOTE_D5,-4,
  NOTE_C5,2, REST,2,

  REST,4, NOTE_G5,8, NOTE_FS5,8, NOTE_F5,8, NOTE_DS5,4, NOTE_E5,8,
  REST,8, NOTE_GS4,8, NOTE_A4,8, NOTE_C4,8, REST,8, NOTE_A4,8, NOTE_C5,8, NOTE_D5,8,
  REST,4, NOTE_DS5,4, REST,8, NOTE_D5,-4,
  NOTE_C5,2, REST,2,
};

byte tempo = 200;

short notes = sizeof(melody) / sizeof(melody[0]) / 2;

short wholenote = (60000 * 4) / tempo;

short divider = 0, noteDuration = 0;

void music() {
  for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {

    divider = melody[thisNote + 1];
    if (divider > 0) {
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5;
    }

    tone(buzzer, melody[thisNote], noteDuration * 0.9);

    delay(noteDuration);

    noTone(buzzer);
  }
}

void loop() {
  if (digitalRead(firstButton) == HIGH) {
    Serial.println("1");
    while(digitalRead(firstButton)) {}
  }
  else if (digitalRead(secondButton) == HIGH) {
    Serial.println("2");
    while(digitalRead(secondButton)) {}
  }

  if (Serial.available() > 0){
    String command = Serial.readString();

    if (command == "0.1") {
      isBlink = true;
      blink = 100;
    } else if (command == "0.5") {
      isBlink = true;
      blink = 500;
    } else if (command == "1") {
      isBlink = true;
      blink = 1000;
    } else if (command == "-1") {
      music();
    } else {
    }
  }

  if (isBlink) {
    digitalWrite(led, HIGH);
    delay(blink);
    digitalWrite(led, LOW);
    delay(blink);
  }

  delay(50);
}