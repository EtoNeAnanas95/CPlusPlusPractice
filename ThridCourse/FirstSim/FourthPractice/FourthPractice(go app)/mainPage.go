package main

import (
	"fmt"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"log"
)

func LoadMainPage() {
	var names []string
	_ = names

	output := widget.NewLabel("Wait for button pressed")
	//go logPort()
	blinkFrequencyCombobox := widget.NewSelect([]string{"0.1", "0.5", "1"}, func(s2 string) {

	})
	blinkFrequencyCombobox.SetSelected("0.1")

	startBlinkingBtn := widget.NewButton("Start blinking led", func() { go startBlinking(blinkFrequencyCombobox) })
	playMusicBtn := widget.NewButton("Play music", func() { go playMusic() })

	btnsContainer := container.NewHBox(startBlinkingBtn, playMusicBtn)
	mainWindow.SetContent(container.NewCenter(
		container.NewVBox(
			container.NewCenter(output),
			container.NewCenter(blinkFrequencyCombobox),
			container.NewCenter(btnsContainer),
		),
	),
	)
}

func startBlinking(cb *widget.Select) {
	fmt.Println(cb.Selected)
	s.Write([]byte(cb.Selected))
}

func playMusic() {
	message := "-1\n"
	s.Write([]byte(message))
}

func logPort() {
	buf := make([]byte, 128)
	for {
		n, err := s.Read(buf)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(buf[:n]))
	}
}
