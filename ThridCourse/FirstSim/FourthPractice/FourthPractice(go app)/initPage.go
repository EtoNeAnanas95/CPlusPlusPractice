package main

import (
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"github.com/tarm/serial"
)

func LoadInitPage() {
	arduinoPath := widget.NewEntry()
	arduinoPath.SetPlaceHolder("type path to the Arduino device")
	status := widget.NewLabel("wait for path")
	arduinoPath.OnSubmitted = func(path string) {
		c := &serial.Config{Name: path, Baud: 9600}
		serialPort, err := serial.OpenPort(c)
		if err != nil {
			status.SetText("can not submit path")
		} else {
			s = serialPort
			LoadMainPage()
		}
	}
	mainWindow.SetContent(container.NewGridWithRows(4,
		container.NewVBox(),
		arduinoPath,
		container.NewCenter(status),
		container.NewVBox()))
}
