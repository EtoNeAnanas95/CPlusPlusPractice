package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"github.com/tarm/serial"
	"log/slog"
	"os"
	"time"
)

var ledFrequency time.Duration
var mainWindow fyne.Window
var logger *slog.Logger
var s *serial.Port

func main() {
	opt := &slog.HandlerOptions{}
	opt.Level = slog.LevelDebug
	logger := slog.New(slog.NewTextHandler(os.Stdout, opt))

	logger.Info("Start Ui app")

	logger.Info("Create new UI app")
	UI := app.New()

	mainWindow = UI.NewWindow("Main window")

	LoadInitPage()

	logger.Info("Show and run UI app")
	go mainWindow.ShowAndRun()

}
