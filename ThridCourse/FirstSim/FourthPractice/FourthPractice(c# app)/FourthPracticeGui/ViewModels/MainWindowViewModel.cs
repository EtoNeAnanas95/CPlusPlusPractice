﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Ports;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using FourthPracticeGui.Views.Pages;
using Timer = System.Timers.Timer;

namespace FourthPracticeGui.ViewModels;

public class MainWindowViewModel : INotifyPropertyChanged
{
    private object _content;
    private string _message;

    public float TimingSelect
    {
        get => _timingSelect;
        set => SetField(ref _timingSelect, value);
    }

    private float _timingSelect;

    public ObservableCollection<float> Timings
    {
        get => _timings;
        set => SetField(ref _timings, value);
    }

    private ObservableCollection<float> _timings = new();

    private string _portName;

    private SerialPort _serialPort;

    private Timer _timer;

    public MainWindowViewModel()
    {
        Content = new FirstContent();
        Message = "Output";
        Timings.Add(0.1f);
        Timings.Add(0.5f);
        Timings.Add(1f);
    }

    public object Content
    {
        get => _content;
        set => SetField(ref _content, value);
    }

    public string Message
    {
        get => _message;
        set => SetField(ref _message, value);
    }

    public string PortName
    {
        get => _portName;
        set => SetField(ref _portName, value);
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    public void SubmitPort()
    {
        Console.WriteLine("PortName: " + PortName);

        if (_serialPort != null && _serialPort.IsOpen)
        {
            _serialPort.Close();
            Console.WriteLine("Disconnect");
            return;
        }

        try
        {
            _serialPort = new SerialPort(PortName, 9600)
            {
                DtrEnable = true,
                ReadTimeout = 1000
            };

            Thread.Sleep(500);
            // _serialPort.Open();
            Content = new MainContent();
            Console.WriteLine("Connected");
        }
        catch (Exception e)
        { return; }

        _timer = new Timer(1000);
        _timer.Elapsed += OnTimerEvent;
        _timer.AutoReset = true;
        _timer.Enabled = true;

        // CommandName = "Отключиться";
    }

    public void LedBlink()
    {
        if (TimingSelect != 0 && _serialPort != null && _serialPort.IsOpen)
        {
            _serialPort.Write(TimingSelect.ToString());
            Console.WriteLine(TimingSelect.ToString());
        }
    }

    public void OnTimerEvent(object sender, ElapsedEventArgs e)
    {
        if (!_serialPort.IsOpen) return;

        try
        {
            _serialPort.DiscardInBuffer();
            var textInPort = _serialPort.ReadLine();
            Message = textInPort;
        }
        catch (Exception exception)
        { Console.WriteLine(exception); }
    }

    public void PlayMusic()
    {
        if (_serialPort != null && _serialPort.IsOpen)
        {
            _serialPort.Write("-1");
        }
    }

    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }
}