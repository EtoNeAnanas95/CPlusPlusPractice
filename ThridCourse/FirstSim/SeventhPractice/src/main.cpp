#include <Arduino.h>
#include <string.h>
#include <LiquidCrystal_I2C.h>

#define DRIVER_VERSION 1
LiquidCrystal_I2C lcd(DRIVER_VERSION ? 0x27 : 0x3f, 16, 2);

#define DEGREE 223

#define PROGRESS_BAR_LEFT_CAP 0
#define PROGRESS_BAR_EMPTY_SEGMENT 1
#define PROGRESS_BAR_FULL_SEGMENT 2
#define PROGRESS_BAR_RIGHT_CAP 3

byte PROGRESS_BAR_LEFT_CAP_DATA[8] = {0b11111, 0b10000, 0b10000, 0b10000, 0b10000, 0b10000, 0b10000, 0b11111};
byte PROGRESS_BAR_EMPTY_SEGMENT_DATA[8] = {0b11111, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111};
byte PROGRESS_BAR_FULL_SEGMENT_DATA[8] = {0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111};
byte PROGRESS_BAR_RIGHT_CAP_DATA[8] = {0b11111, 0b00001, 0b00001, 0b00001, 0b00001, 0b00001, 0b00001, 0b11111};

char inData[108];
int PCdata[26];
byte index = 0;
unsigned long timeout;
bool clearDisplayFlag = true, connectLostFlag = true;
bool updateDisplayFlag;

void createSymbols()
{
    lcd.createChar(PROGRESS_BAR_LEFT_CAP, PROGRESS_BAR_LEFT_CAP_DATA);
    lcd.createChar(PROGRESS_BAR_EMPTY_SEGMENT, PROGRESS_BAR_EMPTY_SEGMENT_DATA);
    lcd.createChar(PROGRESS_BAR_FULL_SEGMENT, PROGRESS_BAR_FULL_SEGMENT_DATA);
    lcd.createChar(PROGRESS_BAR_RIGHT_CAP, PROGRESS_BAR_RIGHT_CAP_DATA);
}

void drawLoadDisplay()
{
    lcd.setCursor(4, 0);
    lcd.print("CPU TEMP");
    lcd.setCursor(2, 1);
    lcd.print("by Pineapples");
    delay(3000);
    lcd.clear();
}

void getCpuTemp()
{
    while (Serial.available() > 0)
    {
        char aChar = Serial.read();
        if (aChar == 'E')
        {
            char* p = inData;
            char* str;
            index = 0;
            while ((str = strtok_r(p, ";", &p)) != NULL)
            {
                PCdata[index++] = atoi(str);
            }
            index = 0;
            updateDisplayFlag = true;
        }
        else
        {
            inData[index++] = aChar;
            inData[index] = '\0';
        }
        timeout = millis();
        connectLostFlag = true;
    }
}

void getCpuTempStub()
{
    updateDisplayFlag = true;
    PCdata[0] = random(897, 900);
    timeout = millis();
    delay(500);
}

void drawCpuTemperature()
{
    lcd.setCursor(1, 0);
    lcd.print("Hi hi ha ha");
    lcd.setCursor(1, 1);
    lcd.print("CPU:");
    lcd.print(PCdata[0]);
    lcd.write(DEGREE);

    byte line = min((byte)ceil(PCdata[0] / 16), (byte)6);
    lcd.setCursor(9, 1);

    lcd.write(line == 0 ? PROGRESS_BAR_LEFT_CAP : PROGRESS_BAR_FULL_SEGMENT);
    for (int n = 1; n < 5; n++)
    {
        lcd.write(n < line ? PROGRESS_BAR_FULL_SEGMENT : PROGRESS_BAR_EMPTY_SEGMENT);
    }
    lcd.write(line == 6 ? PROGRESS_BAR_FULL_SEGMENT : PROGRESS_BAR_RIGHT_CAP);
}

void updateDisplay()
{
    if (updateDisplayFlag)
    {
        if (clearDisplayFlag)
        {
            lcd.clear();
            clearDisplayFlag = false;
        }
        drawCpuTemperature();
        updateDisplayFlag = false;
    }
}

void checkConnect()
{
    if (millis() - timeout > 5000 && connectLostFlag)
    {
        lcd.clear();
        lcd.setCursor(4, 0);
        lcd.print("COM PORT");
        lcd.setCursor(2, 1);
        lcd.print("DISCONNECTED");
        clearDisplayFlag = true;
        connectLostFlag = false;
    }
}

void setup()
{
    Serial.begin(9600);
    lcd.init();
    lcd.backlight();
    lcd.clear();
    createSymbols();
    drawLoadDisplay();
}

void loop()
{
    getCpuTempStub();
    updateDisplay();
    checkConnect();
}