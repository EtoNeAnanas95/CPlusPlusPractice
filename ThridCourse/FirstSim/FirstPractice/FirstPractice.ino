#define b 10
#define g 11
#define r 9

void setup() {
  // put your setup code here, to run once:
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);
}

void loop() {
  analogWrite(r, 255);
  delay(1000);
  analogWrite(r, 0);
  delay(1000);
  analogWrite(r, 255);
  analogWrite(g, 255);
  delay(1000);
  analogWrite(r, 0);
  analogWrite(g, 0);
  delay(1000);
  analogWrite(g, 255);
  delay(1000);
  analogWrite(g, 0);
  delay(1000);
  
  analogWrite(g, 255);
  delay(300);
  analogWrite(g, 0);
  delay(300);
  analogWrite(g, 255);
  delay(300);
  analogWrite(g, 0);
  delay(300);
  analogWrite(g, 255);
  delay(300);
  analogWrite(g, 0);
  delay(300);
  
  analogWrite(r, 255);
  analogWrite(g, 255);
  delay(1000);
  analogWrite(r, 0);
  analogWrite(g, 0);
  delay(1000);
  
}