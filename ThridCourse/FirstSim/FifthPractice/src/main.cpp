#include <Arduino.h>
#include <Wire.h>

#define ledPin 11
byte newBrightness = 2;
byte tempBrightness = 0;
bool timer = false;
bool timerIsEnd = false;
short time = 0;
short secondTime = 500;

void receiveEvent(int bytes)
{
    while (Wire.available())
    {
        auto data = Wire.read();
        tempBrightness = newBrightness;
        newBrightness = data;
        if (newBrightness > 2 && tempBrightness < 2)
        {
            Serial.println("timer start");
            timer = true;
        }
    }
    analogWrite(ledPin, newBrightness);
}

void requestEvent()
{
    if (timerIsEnd && newBrightness > 2)
    {
        Serial.println("send info that timer is stop");
        Wire.write(true);
        timerIsEnd = false;
    } else Wire.write(false);
}

void setup()
{
    Serial.begin(9600);
    Wire.begin(8);
    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);
    pinMode(ledPin, OUTPUT);
    Serial.println("Listen...");
}

void loop()
{
    if (timer)
    {
        time += 100;
        if (time > 5000)
        {
            time = 0;
            timerIsEnd = true;
            timer = false;
        }
    }
    if (timerIsEnd && secondTime > 0) secondTime -= 100;
    else if (timerIsEnd)
    {
        timerIsEnd = false;
        secondTime = 500;
        Serial.println("timer stop");
    }
    delay(100);
}
