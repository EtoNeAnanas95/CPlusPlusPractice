#include <Arduino.h>
#include <pitches.h>
#define latchPin 8
#define clockPin 12
#define dataPin 11
#define buzzer 6

short num = 0;
short timer = 1100;

const byte numberBits[] = {
    B11111111,
    B00000110,
    B01011011,
    B01001111,
    B11100110,
    B01101101,
    B01111101,
    B00000111,
    B01111111,
    B01101111
};

void setup()
{
    Serial.begin(9600);

    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT);
    pinMode(dataPin, OUTPUT);

    Serial.println("Starting up...");
}

int melody[] = {
    NOTE_E5, 8, NOTE_E5, 8, REST, 8, NOTE_E5, 8, REST, 8, NOTE_C5, 8, NOTE_E5, 8,
    NOTE_G5, 4, REST, 4, NOTE_G4, 8, REST, 4,
    NOTE_C5, -4, NOTE_G4, 8, REST, 4, NOTE_E4, -4,
    NOTE_A4, 4, NOTE_B4, 4, NOTE_AS4, 8, NOTE_A4, 4,
    NOTE_G4, -8, NOTE_E5, -8, NOTE_G5, -8, NOTE_A5, 4, NOTE_F5, 8, NOTE_G5, 8,
    REST, 8, NOTE_E5, 4,NOTE_C5, 8, NOTE_D5, 8, NOTE_B4, -4,
    NOTE_C5, -4, NOTE_G4, 8, REST, 4, NOTE_E4, -4,
    NOTE_A4, 4, NOTE_B4, 4, NOTE_AS4, 8, NOTE_A4, 4,
    NOTE_G4, -8, NOTE_E5, -8, NOTE_G5, -8, NOTE_A5, 4, NOTE_F5, 8, NOTE_G5, 8,
    REST, 8, NOTE_E5, 4,NOTE_C5, 8, NOTE_D5, 8, NOTE_B4, -4,

    REST, 4, NOTE_G5, 8, NOTE_FS5, 8, NOTE_F5, 8, NOTE_DS5, 4, NOTE_E5, 8,
    REST, 8, NOTE_GS4, 8, NOTE_A4, 8, NOTE_C4, 8, REST, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_D5, 8,
    REST, 4, NOTE_DS5, 4, REST, 8, NOTE_D5, -4,
    NOTE_C5, 2, REST, 2,

    REST, 4, NOTE_G5, 8, NOTE_FS5, 8, NOTE_F5, 8, NOTE_DS5, 4, NOTE_E5, 8,
    REST, 8, NOTE_GS4, 8, NOTE_A4, 8, NOTE_C4, 8, REST, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_D5, 8,
    REST, 4, NOTE_DS5, 4, REST, 8, NOTE_D5, -4,
    NOTE_C5, 2, REST, 2,
};

byte tempo = 200;

bool playing = true;

short notes = sizeof(melody) / sizeof(melody[0]) / 2;

short wholenote = (60000 * 4) / tempo;

short divider = 0, noteDuration = 0;

void music()
{
    for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2)
    {
        divider = melody[thisNote + 1];
        if (divider > 0)
        {
            noteDuration = (wholenote) / divider;
        }
        else if (divider < 0)
        {
            noteDuration = (wholenote) / abs(divider);
            noteDuration *= 1.5;
        }

        tone(buzzer, melody[thisNote], noteDuration * 0.9);

        delay(noteDuration);

        noTone(buzzer);
    }
}


void Timer()
{
    if (num >= 0 && num <= 10)
    {
        if (num == 10)
            shiftOut(dataPin, clockPin, MSBFIRST, numberBits[0]);
        else
            shiftOut(dataPin, clockPin, MSBFIRST, numberBits[num]);
    }
    else
        Serial.println("Error: Number out of range");

    if (timer > 0)
        timer -= 100;
    else
    {
        timer = 1000;
        num++;
    }
    if (num == 10 && playing)
    {
        music();
        playing = false;
    }
}

void loop()
{
    digitalWrite(latchPin, LOW);
    Timer();
    digitalWrite(latchPin, HIGH);
    delay(100);
}
