/*******************************************************************
    Телеграм Бот для модуля WeMos D1 R2 c МК ESP8266.
    Предназначен для вкл./выкл. светодиода на плате.
 *******************************************************************/

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <Servo.h>

// Имя и пароль вашей сети Wifi
#define WIFI_SSID "XXXXXX"
#define WIFI_PASSWORD "YYYYYY"
// Телеграм Бот Токен, можно получить у бота @BotFather в Телеграмм
#define BOT_TOKEN "7943981602:AAFHR4z5wod1B_vz-AdUuKXV95j1tfOk5Es"


const unsigned long BOT_MTBS = 1000; // Через сколько времени проверять сообщения

X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime; // Последнее время сканирования сообщения

const int ledPin = D5;
const int servoPin = D6;
int ledStatus = 0;
Servo myServo;

void handleNewMessages(int numNewMessages)
{
    Serial.print("Обработка нового сообщения ");
    Serial.println(numNewMessages);

    for (int i = 0; i < numNewMessages; i++)
    {
        String chat_id = bot.messages[i].chat_id;
        String text = bot.messages[i].text;

        String from_name = bot.messages[i].from_name;
        if (from_name == "")
            from_name = "Guest";

        if (text == "/ledOn")
        {
            digitalWrite(ledPin, HIGH); // Включить светодиод на плате
            ledStatus = 1;
            bot.sendMessage(chat_id, "Светодиод включен - ON", "");
        }

        if (text == "/ledOff")
        {
            ledStatus = 0;
            digitalWrite(ledPin, LOW); // Выключить светодиод на плате
            bot.sendMessage(chat_id, "Светодиод выключен - OFF", "");
        }

        if (text == "/ledChangePower")
        {
            analogWrite(ledPin, 128); // Установить яркость 50%
            bot.sendMessage(chat_id, "Яркость светодиода установлена на 128", "");
        }

        if (text == "/Open")
        {
            myServo.write(180); // Открываем
            bot.sendMessage(chat_id, "Сервопривод установлен на 180 градусов", "");
        }

        if (text == "/Close")
        {
            myServo.write(0); // Закрываем
            bot.sendMessage(chat_id, "Сервопривод установлен на 0 градусов", "");
        }
        if (text == "/status")
        {
            String statusMsg = ledStatus ? "Светодиод включен - ON" : "Светодиод выключен - OFF";
            bot.sendMessage(chat_id, statusMsg, "");
        }

        if (text == "/status")
        {
            if (ledStatus)
            {
                bot.sendMessage(chat_id, "Светодиод включен - ON", "");
            }
            else
            {
                bot.sendMessage(chat_id, "Светодиод выключен - OFF", "");
            }
        }

        if (text == "/start")
        {
            String welcome = "Привет, я Телеграм Бот. Я умею преключать светодиод на модуле WeMos D2 R1 " + from_name +
                ".\n";
            welcome += "А это перечень команд, которые я пока знаю.\n\n";
            welcome += "/ledOn : Переключает светодиод в состояние ON\n";
            welcome += "/ledOff : Переключает светодиод в состояние OFF\n";
            welcome += "/ledChangePower : \n";
            welcome += "/Open : \n";
            welcome += "/Close : \n";
            welcome += "/status : Возвращает текущее состояние светодиода\n";
            bot.sendMessage(chat_id, welcome, "Markdown");
        }
    }
}


void setup()
{
    Serial.begin(115200);
    Serial.println();

    pinMode(ledPin, OUTPUT); // Настраиваем пин ledPin на выход
    delay(10);
    digitalWrite(ledPin, HIGH); // По умолчанию светодиод выключен

    myServo.attach(servoPin);
    myServo.write(0); // Начальное положение

    // attempt to connect to Wifi network:
    configTime(0, 0, "pool.ntp.org"); // get UTC time via NTP
    secured_client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
    Serial.print("Подключение к сети Wifi SSID: ");
    Serial.print(WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(500);
    }
    Serial.print("\nWiFi подключен. IP адрес: ");
    Serial.println(WiFi.localIP());

    // Check NTP/Time, usually it is instantaneous and you can delete the code below.
    Serial.print("Время подключения: ");
    time_t now = time(nullptr);
    while (now < 24 * 3600)
    {
        Serial.print(".");
        delay(100);
        now = time(nullptr);
    }
    Serial.println(now);
}

void loop()
{
    if (millis() - bot_lasttime > BOT_MTBS)
    {
        int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

        while (numNewMessages)
        {
            Serial.println("Получен ответ");
            handleNewMessages(numNewMessages);
            numNewMessages = bot.getUpdates(bot.last_message_received + 1);
        }

        bot_lasttime = millis();
    }
}
