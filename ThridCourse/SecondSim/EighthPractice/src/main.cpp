#include <Arduino.h>
#include <Servo.h>
#include <Stepper.h>

#define POTENTIOMETER_PIN A0
#define SERVO_PIN 6

const byte STEPS = 32;

Stepper stepperMotor(STEPS, 8, 9, 10, 11);
Servo servo;

void setup() {
    Serial.begin(9600);
    servo.attach(SERVO_PIN);
}

void loop() {
    int potValue = analogRead(POTENTIOMETER_PIN);
    servo.write(map(potValue, 0, 1023, 0, 180));
    stepperMotor.setSpeed(map(servo.read(), 0, 180, 600, 1200));
    stepperMotor.step(1);

    Serial.print("Pot Value: ");
    Serial.print(potValue);
    Serial.print(" | Motor Speed: ");
    Serial.println(map(servo.read(), 0, 180, 600, 1200));
}
